<?php
Class Type{

  public $db = '';

  public function __construct(){
    $this->db = new PDO('mysql:host=DATABASEHOST;dbname=DATABASENAME;charset=utf8mb4', 'DATABASEUSER', 'DATABASEPASSWORD');
    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  }

  /**
  * Return all the types of event
  *
  *
  */
  public function getAll(){
    $stmt = $this->db->query("SELECT * FROM types");
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

}

?>
