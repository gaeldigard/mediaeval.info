<?php
Class Event{

  public $db = '';

  public function __construct(){
    $this->db = new PDO('mysql:host=DATABASEHOST;dbname=DATABASENAME;charset=utf8mb4', 'DATABASEUSER', 'DATABASEPASSWORD');
    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  }

  /**
  * Return all the events
  *
  *
  */
  public function getAll(){
    $stmt = $this->db->query("SELECT * FROM events WHERE `is_valid` = 1");
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

  /**
  * Return all the events 3 days around a given date
  *
  *
  */
  public function getEventsBetweenDates($from){

    try{
      $from = $from . ' 00:00:00'; // add time to match DB field - maybe one day we'll use it

      // fuckin' useless PDO. have to do this in PHP instead of MYSQL, because
      // INTERVAL function doesn't work with :params or shit like this
      // I hate PDO...
      $fromPlus7 = date("Y-m-d H:i:s", strtotime("+3 day", strtotime($from)));
      $fromMinus7 = date("Y-m-d H:i:s", strtotime("-3 day", strtotime($from)));

      $stmt = $this->db->prepare("SELECT * FROM `events`
        WHERE date_start BETWEEN :fromMinus7 AND :fromPlus7
        AND `is_valid` = 1
         ");
      //$stmt->bindParam(':from', $from);
      $stmt->bindParam(':fromMinus7', $fromMinus7);
      $stmt->bindParam(':fromPlus7', $fromPlus7);
      $stmt->execute();
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e){
        echo $e->getMessage();
    }

    return $results;
  }

  /**
  * Return all the events containing a given keyword
  *
  *
  */
  public function getEventsByKeyword($keyword){

    $keyword = '%'.urldecode($keyword).'%'; // fucking bindParam() who only accept variable...

    try{
      // look for in : name, desc, city
      $stmt = $this->db->prepare("SELECT * FROM `events`
        WHERE `name` LIKE :keyword1
        OR `desc` LIKE    :keyword2
        OR `city` LIKE    :keyword3
        AND `is_valid` = 1
         ");

      // putain mais PDO je te crame ta famille toi...
      // chaque param doit avoir un nom unique !!!!!
      $stmt->bindParam(':keyword1', $keyword);
      $stmt->bindParam(':keyword2', $keyword);
      $stmt->bindParam(':keyword3', $keyword);

      $stmt->execute();
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e){
        echo $e->getMessage();
    }

    return $results;
  }

  /**
  * Return all the events matching type ID
  *
  *
  */
  public function getEventsByType($type){


    try{

      $stmt = $this->db->prepare("SELECT * FROM `events`
        WHERE `type` LIKE :type AND `is_valid` = 1
         ");

      $stmt->bindParam(':type', $type);
      $stmt->execute();
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e){
        echo $e->getMessage();
    }

    return $results;
  }

  /**
  * Add event in DB
  *
  * @params array event
  */
  public function add($event){
    try {
        $now = date("Y-m-d H:i:s",time());
        // automatic validation for Axelle and I
        $is_valid = 0;
        if( $event['email'] == 'setyourmail@domain.com'  )
          $is_valid = 1;


        // old way... like in 2007... thanks PDO
        $stmt = $this->db->prepare("
          INSERT INTO `events` (`name`, `desc`, `contact`, `address`, `city`,
          `iso_country`, `lat`, `long`,`email`, `website`,`cost`,`date_start`,
          `date_end`,`type`,`lang`,`created_at`,`updated_at`,`is_valid`)
          VALUES (:name, :desc, :contact, :address,
          :city, :iso_country, :lat, :long, :email, :website, :cost, :date_start, :date_end,
          :type, :lang, :created_at, :updated_at, :is_valid
          )");
        $stmt->execute(array(
            ':name' => $event['name'],
            ':desc' => $event['desc'],
            ':contact' => $event['contact'],
            ':address' => $event['address'],
            ':city' => $event['city'],
            ':iso_country' => $event['country'],
            ':lat' => $event['lat'],
            ':long' => $event['long'],
            ':email' => $event['email'],
            ':website' => $event['website'],
            ':cost' => $event['cost'],
            ':date_start' => $event['date_start_submit'],
            ':date_end' => $event['date_end_submit'],
            ':type' => $event['type'],
            ':lang' => $event['lang'],
            ':created_at' => $now,
            ':updated_at' => $now,
            ':is_valid' => $is_valid,
        ));
      }
      catch (PDOException $e) {
          if ($e->getCode() == 1062) {
              // Take some action if there is a key constraint violation, i.e. duplicate name
          } else {
              throw $e;
          }
      }

      $id = $this->db->lastInsertId();

      // if translation selected, let's follow it
      if(isset($_GET['lang'])) $language = '&lang='.$_GET['lang'];
      else $language = '';

      $headers  = "From: Mediaeval.info < noreply@mediaeval.info >\n";
      $headers .= "Cc: Mediaeval.info < noreply@mediaeval.info >\n";
      $headers .= "X-Sender: Mediaeval.info < noreply@mediaeval.info >\n";
      $headers .= 'X-Mailer: PHP/' . phpversion();
      $headers .= "X-Priority: 1\n"; // Urgent message!
      $headers .= "Return-Path: noreply@mediaeval.info\n"; // Return path for errors
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/plain; charset=UTF-8\n";

      mail('setyourmail@domain.com','Nouvel événement','
Nouvel événement ajouté : '.$event[name].'

'.$event[desc].'

'.$event[address].', '.$event[city].', '.$event[country].'

Par : '.$event[contact].', '.$event[email].'
Du '.$event[date_start_submit].' au '.$event[date_end_submit].'

http://mediaeval.info/manage/validate/943724ZljnjkFZEFZNVJJHEjbkjhbmlnvA/'. $id .'

: insert here adresse to validate', $headers);
    header("Location: /?addsuccess=1$language");

  }

}

 ?>
