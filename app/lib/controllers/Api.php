<?php
/**
* API class to return json datas
* TODO : only accessible from a given domain (mine)
*
*
*/
 
Class Api{

  // members
  public $eventInstance = null;

  public function __construct(){

    // require models
    require 'app/lib/models/Event.php';
    require 'app/lib/models/Type.php';

    // making instances
    $this->eventInstance = new Event();
    $this->typeInstance  = new Type();

    // set output content-type
    header('Content-Type: application/json;charset=utf-8');

  }

  //
  // GET
  //

  // get all events
  public function getAllEvents(){
    echo json_encode($this->eventInstance->getAll());
  }

  // get events 3 days around
  public function getEventsBetweenDates($from){
    echo json_encode($this->eventInstance->getEventsBetweenDates($from));
  }

  // get events by keyword
  public function getEventsByKeyword($keyword){
    echo json_encode($this->eventInstance->getEventsByKeyword($keyword));
  }

  // get events by type
  public function getEventsByType($type){
    echo json_encode($this->eventInstance->getEventsByType($type));
  }

  // get all type
  public function getAllTypes(){
    echo json_encode($this->typeInstance->getAll());
  }

}
?>
