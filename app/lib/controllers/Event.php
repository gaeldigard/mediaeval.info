<?php
/**
* Event class used to manage events
*
*
*
*/

Class EventController{

  // members
  public $eventModelInstance = null;
  public $db = '';

  public function __construct(){
    require 'app/lib/models/Event.php';
    // fix a missing function due to a too low php version
    require 'app/vendor/gump/array_column.php';
    $this->eventModelInstance = new Event();

  }

  private function DBconnect(){
    $this->db = new PDO('mysql:host=localhost;dbname=c3medieval_fest;charset=utf8mb4', 'c3medieval', 'ii5goAs63HYl4');
    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  }

  /**
  * Check data, format data, and send them to the model
  * to save a new event in DB
  *
  * @params array $event
  */
  public function add(){

    require 'app/vendor/gump/gump.class.php';
    $gump = new GUMP('fr');
    $_POST = $gump->sanitize($_POST);

    $gump->validation_rules(array(
    	'contact'    => 'required',
    	'name'    => 'required',
    	'email'       => 'required|valid_email',
      'address'    => 'required',
      'city'    => 'required',
      'country'    => 'required',
      'date_start_submit'    => 'required',
      'date_end_submit'    => 'required',
      'website'    => 'valid_url',
      'cost'    => 'required',
      'type'    => 'required',
      'desc'    => 'required',
    ));

    $gump->filter_rules(array(
    	'contact' => 'trim|sanitize_string',
    	'name' => 'trim|sanitize_string',
    	'email'    => 'trim|sanitize_email',
      'address' => 'trim|sanitize_string',
      'city' => 'trim|sanitize_string',
      'country' => 'trim|sanitize_string',
      'date_start_submit' => 'trim|sanitize_string',
      'date_end_submit' => 'trim|sanitize_string',
      'website' => 'trim|sanitize_string',
      'cost' => 'trim|sanitize_string',
      'type' => 'trim|sanitize_string',
      'desc' => 'trim|sanitize_string',

    ));

    $validated_data = $gump->run($_POST);

    if($validated_data === false) {
    	echo $gump->get_readable_errors(true);
      echo '<br/><a href="javascript:history.back()">Retour</a>';
    }
    else { // ça marche bien !

      // we dont need them anymore
      unset($validated_data['date_start']);
      unset($validated_data['date_end']);
      // formating SQL date format
      $validated_data['date_start_submit'] = date("Y-m-d H:i:s",strtotime($validated_data['date_start_submit']));
      $validated_data['date_end_submit'] = date("Y-m-d H:i:s",strtotime($validated_data['date_end_submit']));
      // adding some infos
      $validated_data['lang'] = 1;
      //geo code
      $geocoded = $this->geocodeAddress($validated_data['address'] . ',' . $validated_data['city'] .',' . $validated_data['country']);

      $validated_data['lat'] = $geocoded[0];
      $validated_data['long'] = $geocoded[1];
      $validated_data['address'] = $geocoded[2];
      // send to the model to save
      $this->eventModelInstance->add($validated_data);

    }

  }

  // debug function to update LAT LONG of mass added events from the net
  public function updateEventsPosition(){
    echo 'ok';

    $this->DBconnect();
    $stmt = $this->db->query("SELECT * FROM events WHERE `lat` = 0 LIMIT 201, 300");
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($results as $event){
      $geocoded = $this->geocodeAddress($event['city'].', '.$event['iso_country']);
      $event['lat'] = $geocoded[0];
      $event['long'] = $geocoded[1];

      // update DB
      $UpdateStmt = $this->db->prepare("
          UPDATE `events`
          SET `lat`=:lat, `long`=:long
          WHERE `id`=:id
       ");

       $UpdateStmt->execute(array(':lat'=>$event['lat'], ':long'=>$event['long'], ':id' => $event['id']));

       if($UpdateStmt->rowCount()) {
         echo 'success';
       } else {
         echo 'update failed id : '.$event['id'];
       }


    }
  }

  /**
  * Admin / managing function to validate a fest
  * for now it use $created_at as a primary key
  * but the best would be the event ID
  *
  */
  public function validate($id){

    $this->DBconnect();

    // update DB
    $UpdateStmt = $this->db->prepare("
        UPDATE `events`
        SET `is_valid`=1
        WHERE `id`=:id
     ");

     $UpdateStmt->execute(array(':id'=>$id));

     if($UpdateStmt->rowCount()) {
       echo 1;
       $stmt = $this->db->prepare("SELECT * FROM events WHERE `id` =:id");
       $stmt->bindParam(':id', $id);
       $stmt->execute();
       $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
       foreach($results as $event){
         $to = $event['email'];

         $headers  = "From: Mediaeval.info < noreply@mediaeval.info >\n";
         $headers .= "Cc: Mediaeval.info < noreply@mediaeval.info >\n";
         $headers .= "X-Sender: Mediaeval.info < noreply@mediaeval.info >\n";
         $headers .= 'X-Mailer: PHP/' . phpversion();
         $headers .= "X-Priority: 1\n"; // Urgent message!
         $headers .= "Return-Path: noreply@mediaeval.info\n"; // Return path for errors
         $headers .= "MIME-Version: 1.0\r\n";
         $headers .= "Content-Type: text/plain; charset=UTF-8\n";

         mail($to, 'Validation Mediaeval.info','
Bonjour

Merci pour l\'ajout de votre événement sur Mediaeval.info. Il vient d’être validé !

N’hésitez pas à vérifier s’il est bien placé sur la carte : http://mediaeval.info

Et surtout, n\'hésitez pas à partager l’application, à vos amis, partout dans le monde
(elle est disponible en 20 langues !), et à ajouter autant de fêtes que vous voulez,
celles à venir et même celles déjà passées !

Merci !

Nun est bibendum.
         ', $headers);

       }
     } else {
       echo 'update failed';
     }



  }

  private function geocodeAddress($address){

    // url encode the address
    $address = urlencode($address);

    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

    // get the json response
    $resp_json = file_get_contents($url);

    // decode the json
    $resp = json_decode($resp_json, true);

    // response status will be 'OK', if able to geocode given address
    if($resp['status']=='OK'){

        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];

        // verify if data is complete
        if($lati && $longi && $formatted_address){

            // put the data in the array
            $data_arr = array();

            array_push(
                $data_arr,
                    $lati,
                    $longi,
                    $formatted_address
                );

            return $data_arr;

        }else{
            return false;
        }

    }else{
        return false;
    }
}

}
