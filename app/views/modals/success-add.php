<?php use Translation\Translation as T; ?>
<!-- Modal Structure -->
<div id="modalSuccess" class="modal">
  <div class="modal-content">
    <h4><?= T::of('modal.success.title'); ?></h4>
    <p><?= T::of('modal.success.content'); ?></p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn">ok</a>
  </div>
</div>
