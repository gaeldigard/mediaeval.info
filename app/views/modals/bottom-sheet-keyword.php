<?php use Translation\Translation as T; ?>
<!-- Modal Structure -->
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <h4><?= T::of('modal.keyword.title'); ?></h4>
<form id="form-keyword">
          <input placeholder="<?= T::of('modal.keyword.placeholder'); ?>" id="keyword" type="text" class="align-center validate">
          <label for="first_name"><?= T::of('modal.keyword.label'); ?></label>
</form>
  </div>
  <div class="modal-footer">
    <button href="#!" type="submit" form="form-keyword" class="modal-action modal-close waves-effect waves-green btn">Ok</button>
  </div>
</div>
