<?php use Translation\Translation as T; ?>
<!-- Modal Structure -->
<div id="modal2" class="modal bottom-sheet">
  <div class="modal-content">
    <h4><?= T::of('modal.type.title'); ?></h4>
     <p><?= T::of('modal.type.label'); ?></p>
     <div id="types_go_here"></div>
  </div>
  <div class="modal-footer">
    <button href="#!" type="submit"
    class="modal-action modal-close waves-effect waves-green btn"><?= T::of('cancel'); ?></button>
  </div>
</div>
