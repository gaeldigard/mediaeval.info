<?php use Translation\Translation as T; ?>
<!-- Modal Structure -->
<div id="modalLanguages" class="modal modal-fixed-footer">
  <div class="modal-content">


          <a href="?lang=en-US"   class="waves-effect waves-light btn" title="English"> English <img alt="English" src="/assets/img/flags/GB.png"></a>
          <a href="?lang=fr-FR"  class="waves-effect waves-light btn" title="Français">Français<img alt="Français" src="/assets/img/flags/FR.png"></a>
          <a href="?lang=ru-RU"  class="waves-effect waves-light btn" title="Pусский">Pусский<img alt="Pусский" src="/assets/img/flags/RU.png"></a>
          <a href="?lang=tr-TR"  class="waves-effect waves-light btn" title="Türkçe">Türkçe<img alt="Türkçe" src="/assets/img/flags/TR.png"></a>
          <a href="?lang=se-SE"  class="waves-effect waves-light btn" title="Svenska">Svenska<img alt="Svenska" src="/assets/img/flags/SE.png"></a>
          <a href="?lang=de-DE"  class="waves-effect waves-light btn" title="Deutsch">Deutsch<img alt="Deutsch" src="/assets/img/flags/DE.png"></a>

          <a href="?lang=gr-GR"  class="waves-effect waves-light btn" title="ελληνική">ελληνική<img alt="ελληνική" src="/assets/img/flags/GR.png"></a>
          <a href="?lang=pt-PT"  class="waves-effect waves-light btn" title="Português">Português<img alt="Português" src="/assets/img/flags/PT.png"></a>
          <a href="?lang=es-ES"  class="waves-effect waves-light btn" title="Español">Español<img alt="Español" src="/assets/img/flags/ES.png"></a>
          <a href="?lang=ge-GE"  class="waves-effect waves-light btn" title="ქართული">ქართული<img alt="ქართული" src="/assets/img/flags/GE.png"></a>
          <a href="?lang=dk-DK"  class="waves-effect waves-light btn" title="Dansk">Dansk<img alt="Dansk" src="/assets/img/flags/DK.png"></a>
          <a href="?lang=pl-PL"  class="waves-effect waves-light btn" title="Polski">Polski<img alt="Polski" src="/assets/img/flags/PL.png"></a>

          <a href="?lang=sk-SK"  class="waves-effect waves-light btn" title="Slovenský">Slovenský<img alt="Slovenský" src="/assets/img/flags/SK.png"></a>
          <a href="?lang=fi-FI"  class="waves-effect waves-light btn" title="Suomalainen">Suomalainen<img alt="Suomalainen" src="/assets/img/flags/FI.png"></a>
          <a href="?lang=ua-UA"  class="waves-effect waves-light btn" title="Українська">Українська<img alt="Українська" src="/assets/img/flags/UA.png"></a>
          <a href="?lang=cz-CZ"  class="waves-effect waves-light btn" title="Czech">Czech<img alt="Czech" src="/assets/img/flags/CZ.png"></a>
          <a href="?lang=cz-CZ"  class="waves-effect waves-light btn" title="Românesc">Românesc<img alt="Românesc" src="/assets/img/flags/RO.png"></a>
          <a href="?lang=ar-AR"  class="waves-effect waves-light btn" title=" العربية "> العربية<img alt="العربية" src="/assets/img/flags/arab-league.png"/> </a>

          <a href="?lang=it-IT"  class="waves-effect waves-light btn" title="Italiano">Italiano <img alt="Italiano" src="/assets/img/flags/IT.png"/> </a>
          <a href="?lang=hu-HU"  class="waves-effect waves-light btn" title="Magyar">Magyar <img alt="Magyar" src="/assets/img/flags/HU.png"/> </a>
  </div>
  <div class="modal-footer">
    <button href="#!"  class="modal-action modal-close waves-effect waves-red btn red"><?= T::of('cancel'); ?></button>
  </div>
</div>
