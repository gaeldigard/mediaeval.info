<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'app/lib/models/Event.php';

 ?>
<!doctype html>
<html class="no-js" lang="<?=$lang?>" <?php if('ar-AR' == $lang) echo 'dir="rtl"' ?>>
    <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109529040-1"></script>
      <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-109529040-1');
      </script>


        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Mediaeval.info - Toutes les fêtes médiévales en France et en Europe</title>
        <meta name="description" content="Carte des fêtes médiévales en France et en Europe">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->

        <link rel="stylesheet" href="assets/css/materialize.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->


        <?php include_once 'app/views/modals/bottom-sheet-keyword.php'; ?>
        <?php include_once 'app/views/modals/bottom-sheet-type.php'; ?>
        <?php include_once 'app/views/modals/bottom-sheet-languages.php'; ?>
        <?php include_once 'app/views/modals/success-add.php'; ?>

           <?php include_once 'app/views/sidebar.php'; ?>
          <div id="map"></div>
          <input type="hidden" id="filterDate2" />


        <script  defer
              src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdfs-sYKbGcA9TpasuagjBVvCTpS0nM1g&callback=initMap">
        </script>
        <script src="assets/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="assets/js/vendor/materialize.min.js"></script>
        <script src="assets/js/vendor/moment.js"></script>
        <script src="assets/js/vendor/moment.FR-fr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/api.js"></script>


    </body>
</html>
