<!doctype html>
<html class="no-js" lang="">
    <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109529040-1"></script>
      <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-109529040-1');
      </script>
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
      <link rel="manifest" href="/manifest.json">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="theme-color" content="#ffffff">

      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>About Mediaeval.info</title>
      <meta name="description" content="All you need to know about Mediaeval.info">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->

      <link rel="stylesheet" href="assets/css/materialize.min.css">
      <link rel="stylesheet" href="assets/css/main.css">

  </head>
  <body>
    <div class="container">
      <h1>About</h1>
      Started in Novembre 2017 by Gaël DIGARD, helped by Анна Шевченко and her friends for translations, and Axelle Wolf for feeding the database.
      Proud to use materializecss for a quick development and a better mobile compatibility.<br/>
      Contact : gaeldigard@gmail.com <br/>
      <h1>Thanks to</h1>
      <ul class="collection with-header">
        <li class="collection-header"><h4>The translators</h4></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/RU.png" alt="" class="circle"><span class="title">Russian</span><p>Анна Шевченко</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/TR.png" alt="" class="circle"><span class="title">Turkish</span><p> Sinan Tecer</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/SE.png" alt="" class="circle"><span class="title">Swedish</span><p> Johan Larsson Hörkén</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/DE.png" alt="" class="circle"><span class="title">German</span><p> Simon Urbanek</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/GR.png" alt="" class="circle"><span class="title">Greek</span><p> Constantinos Koushiappis</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/PT.png" alt="" class="circle"><span class="title">Portuguese</span><p> Hugo Silva</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/ES.png" alt="" class="circle"><span class="title">Spanish</span><p> Mariana Hernández</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/GE.png" alt="" class="circle"><span class="title">Georgian</span><p> David Vardosanidze</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/DK.png" alt="" class="circle"><span class="title">Danish</span><p> Jan Paul</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/PL.png" alt="" class="circle"><span class="title">Polish</span><p> Piotr Kuliczkowski</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/SK.png" alt="" class="circle"><span class="title">Slovak</span><p> Daniel Bicák</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/FI.png" alt="" class="circle"><span class="title">Finnish</span><p> Antti Viktor Rauhala</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/UA.png" alt="" class="circle"><span class="title">Ukrainian</span><p> Анна Шевченко</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/CZ.png" alt="" class="circle"><span class="title">Czeck</span><p> Vítek Maca</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/RO.png" alt="" class="circle"><span class="title">Romanian</span><p> Razvan Antoniu</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/arab-league.png" alt="" class="circle"><span class="title">Arabic</span><p> Ali Majrabi</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/IT.png" alt="" class="circle"><span class="title">Italian</span><p> Davide Bosotti</p></li>
        <li class="collection-item avatar"><img src="/assets/img/flags/HU.png" alt="" class="circle"><span class="title">Hungarian</span><p> Tamas Barakony</p></li>
      </ul>
      </div>
    </body>
</html>
