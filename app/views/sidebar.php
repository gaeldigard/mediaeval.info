<?php use Translation\Translation as T;
 ?>
 <ul id="slide-out" class="side-nav">
    <li class="center-align"><div class="user-view">
      <a href="#!name"><span class="black-text  app-name">Mediaeval.info</span></a>
      <div class="center-align"><img src="assets/img/logo-medieval-100.png"></div>

      <a href="#!name"><span class="black-text app-desc center-align">
        <?= T::of('sidebar.presentation'); ?>
      </span></a>

     </div></li>
    <li><a class="subheader"><i class="tiny material-icons">filter_list</i><?= T::of('sidebar.filters'); ?>...

    </a>
    </li>

    <li class="reset"><a href="#!">
      <div class="chip  " >
          <?= T::of('sidebar.filters.reset'); ?>
      </div></a>
    </li>

    <li><a class="waves-effect modal-trigger" href="#modal1"><i class="tiny material-icons">search</i><?= T::of('sidebar.filters.keyword'); ?></a></li>
    <li><a class="waves-effect" href="#!" id="filterDate"><i class="tiny material-icons">access_time</i><?= T::of('sidebar.filters.date'); ?></a></li>
    <li><a class="waves-effect modal-trigger" href="#modal2"><i class="tiny material-icons">subject</i><?= T::of('sidebar.filters.type'); ?></a></li>

    <!--<li><div class="divider"></div></li>-->

    <li><a class="subheader"><i class="tiny material-icons">settings</i><?= T::of('sidebar.settings'); ?></a></li>
    <li><a class="waves-effect" href="/ajouter<?php if(isset($_GET['lang'])) echo '?lang='.$_GET['lang']; ?>"><i class="tiny material-icons">add</i><?= T::of('sidebar.settings.add'); ?></a></li>
    <!--<li><a class="waves-effect" href="#!"><i class="tiny material-icons">edit</i>Modifier votre événement</a></li>
-->
    <!--<li><div class="divider"></div></li>-->
    <li><a class="waves-effect" href="mailto:gaedigard@gmail.com"><i class="tiny material-icons">mail_outline</i><?= T::of('sidebar.settings.contact'); ?></a></li>

    <li><a class="waves-effect modal-trigger" href="#modalLanguages"><i class="tiny material-icons">language</i> Lang </a></li>

    <li><a class="waves-effect" href="/about"><i class="tiny material-icons">info_outline</i>About</a></li>

    <div class="center-align  lang-select">
      <div class="pun"><?= T::of('pun'); ?> </div>


    </div>
    <div class="center-align about">
      <a href="/about" title="Info"><i class="tiny material-icons"></i></a>
    </div>
  </ul>
  <a href="#" data-activates="slide-out" class="button-collapse menu"><img src="assets/img/logo-medieval-100.png" style="width:100%;"/></a>
