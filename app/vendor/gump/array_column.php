<?php
  if(!function_exists('array_column')){
  function array_column($array,$colname,$Indexkey=''){
    $return_array = array();
    if(is_array($array) || is_object($array)){
      foreach($array as $arrayDATA){
        if(is_object($arrayDATA)){
          if(isset($arrayDATA->{$colname})){
            if(isset($Indexkey) && isset($arrayDATA->{$Indexkey}) ){
              $return_array[$arrayDATA->{$Indexkey}] = $arrayDATA->{$colname};
            } else {
              $return_array[] = $arrayDATA->{$colname};
            }
          }
        } else if(is_array($arrayDATA)) {
          if(isset($arrayDATA[$colname])){
            if(isset($Indexkey) && isset($arrayDATA[$Indexkey]) ){
              $return_array[$arrayDATA[$Indexkey]] = $arrayDATA[$colname];
            } else {
              $return_array[] = $arrayDATA[$colname];
            }
          }
        }
      }
    }
    return $return_array;
  }
  }
?>
