/**
* Calling API file
* 8 novembre 2017
* Gaël DIGARD
*/

var api = {};

api.marker = new Array();
api.infoWindow = new Array();

//
// Get all events
//
api.getAllEvents = function(){

    // get events, via API
    $.ajax({
      url: 'api/events/all'
    }).done(function(data){

      api.showEventsOnMap(data);

    });
}

//
// Get events according to a date
//
api.getEventsByDate = function(date){


  // get events via API
  $.ajax({
    url: 'api/events/date/'+date
  }).done(function(data){

    api.showEventsOnMap(data);
    $("#sidenav-overlay").trigger("click");

  });

}

//
// Get events in one given keyword / name
//
api.getEventsByKeyword = function(keyword){

  // get events via API
  $.ajax({
    url: 'api/events/keyword/'+keyword
  }).done(function(data){

    api.showEventsOnMap(data);
    $("#sidenav-overlay").trigger("click");


  });


}

api.getAllTypes = function(){
  //types_go_here
  // get events via API
  $.ajax({
    url: 'api/types/all/'
  }).done(function(data){

    $.each(data, function(i, type) {
      $("#types_go_here").append('<div class="chip" data-type="'+type.id+'">'+type.name+'</div>')
    })

  });
}

api.getEventsByType = function(type){

  // get events via API
  $.ajax({
    url: 'api/events/type/'+type
  }).done(function(data){

    api.showEventsOnMap(data);
    $("#sidenav-overlay").trigger("click");
    $("#modal2 .modal-close").click();

  });
}

//
// Delete all markers
//
api.deleteMarkers = function(){
  for (var i = 0; i < api.marker.length; i++) {
    api.marker[i].setMap(null);
  }
  api.marker = []
}

//
// Place each marker on the map
//
api.showEventsOnMap = function(data){
  $.each(data, function(i, event) {


    // date checking - setting old events in grey - axelle's idea
    var a = moment(event.date_end);
    var b = moment();
    if(a.diff(b)<0) // 86400000
      var image = 'http://mediaeval.info/assets/img/grey-dot.png';
    else
        var image = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';

    api.marker[i] = new google.maps.Marker({
      position: {lat: parseFloat(event.lat), lng: parseFloat(event.long)},
      map: map,
      icon:image
    });

    var desc = '<h2>' + event.name + '</h2>' + '<h3>Du '  + moment(event.date_start).format("DD MMMM YYYY") +
    ' au ' + moment(event.date_end).format("DD MMMM YYYY") + '</h3>'
    desc = desc + ''  + event.address + ' <br/> ';
    desc = desc + 'Tarif : '  + event.cost + ' ';

    if(event.website != '' )
      desc = desc + '- <a href="'+event.website+'" target="_blank">Site web</a><br/>';
    desc = desc + '<p>' + event.desc + '</p>';

    api.infoWindow[i] = new google.maps.InfoWindow({
      content: desc
    });
    api.marker[i].addListener('click', function() {
      api.infoWindow[i].open(map, api.marker[i]);
    });

  }); // end each
}
