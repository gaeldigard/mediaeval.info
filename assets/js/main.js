var map, resultsFiltered, picker;

function initMap() {

  var coordonnees = {lat: 48.452221, lng: -2.047222};
  // dinan 48.452221	-2.047222

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat: 47.1910351, lng: 1.8051638},
    disableDefaultUI: true,
    zoom:5,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  });

  startEngine();

}

/*
* All functions needed to  start the app, at the begining
*
o_______________}o{
|              |   \
|    911       |____\_____
| _____        |    |_o__ |
[/ ___ \       |   / ___ \|
[]_/.-.\_\______|__/_/.-.\_[]
  |(O)|             |(O)|
   '-'   ScS         '-'
---   ---   ---   ---   ---   ---
*/
function startEngine(){
  $('.modal').modal();
  initMenu();
  initDatePicker();
  searchKeyword();
  searchType();
  resetAllFilters();
  api.getAllEvents();
  api.getAllTypes();
  checkSuccess();
  $('select.langSelector').material_select();
 }

function resetAllFilters(){
  $('.reset').click(function(){
    api.deleteMarkers();
    api.getAllEvents();
    $('.reset').fadeOut();
  });
}

/**
* Show a modal if adding an event was a success
*
*/
function checkSuccess(){
  if(findGetParameter('addsuccess') == 1)
     $('#modalSuccess').modal('open');
}

function initMenu(){
    // Initialize collapse button
    $(".button-collapse").sideNav({
       draggable: true
    });
   // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  // $('.collapsible').collapsible();
}

function searchType(){
  $('#types_go_here').on("click", '.chip', function(){
    var t = $(this);
    var type_id = t.attr('data-type');
    api.deleteMarkers();
    api.getEventsByType(type_id);
    $('.reset').fadeIn();

  })
}

function searchKeyword(){
  $('#form-keyword').submit(function(){
    var keyword = $('#keyword').val();
    api.deleteMarkers();
    api.getEventsByKeyword(keyword);
    $('.reset').fadeIn();
  });
}



/**
* Translation and trigger outside the navbar
*
*/
function initDatePicker(){
  if( findGetParameter('lang') == 'ru-RU' ){
    // Russian
    jQuery.extend( jQuery.fn.pickadate.defaults, {
      monthsFull: [ 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря' ],
      monthsShort: [ 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек' ],
      weekdaysFull: [ 'воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота' ],
      weekdaysShort: [ 'вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб' ],
      today: 'сегодня',
      clear: 'удалить',
      close: 'закрыть',
      firstDay: 1,
      format: 'd mmmm yyyy г.',
      formatSubmit: 'yyyy/mm/dd'
    });

    jQuery.extend( jQuery.fn.pickatime.defaults, {
      clear: 'удалить'
    });

  }
  else if ( findGetParameter('lang') == 'fr-FR' ){
    // Extend the default picker options for all instances.
    $.extend($.fn.pickadate.defaults, {
    monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    today: 'aujourd\'hui',
    clear: 'effacer',
    formatSubmit: 'yyyy/mm/dd'
    })
  }
   var $input = $('#filterDate2').pickadate({
     closeOnSelect: true
   })
  picker = $input.pickadate('picker');
  $('#filterDate').click(function(e){
     e.preventDefault();
     e.stopPropagation()
     picker.open();
  })

  // on date select
  picker.on('close', function() {

    var selectedDate = picker.get('select');

    // if a date was effectively selected
    if( '' != selectedDate ){
      var unixDate = selectedDate.year+'-0'+selectedDate.month+'-0'+selectedDate.day;
      // clean the map
      api.deleteMarkers();
      api.getEventsByDate(unixDate);
      $('.reset').fadeIn();
      // empty datepicker
      $('[name="_submit"]').val('');

    }
  })
}
/**
* Get a GET parameter
*/
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
