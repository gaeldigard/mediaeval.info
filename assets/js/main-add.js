$(document).ready(function() {
  $('select#country').material_select();
  $.extend($.fn.pickadate.defaults, {
  monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
  weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
  today: 'aujourd\'hui',
  clear: 'effacer',
  formatSubmit: 'yyyy/mm/dd'
  })
  $('.datepicker').pickadate({
    closeOnSelect: true
  })

  // get events via API
  $.ajax({
    url: 'api/types/all/'
  }).done(function(data){

    $.each(data, function(i, type) {
      $(".event-type-list select").append(' <option value="'+type.id+'">'+type.name+'</option> ')
       $('.event-type-list select').material_select();
    })

  });

});
