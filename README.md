# Mediaeval.info
## Multilang webapp for events displaying and management

*Demo : http://mediaeval.info*

This webapp was developped to list all medieval festivals in the world on a map (Google map here, but works with others).  
But it can fit any kind of event. Feel free to extend. The app comes with 20 translations.

### Database model
Upload the `database.sql` to your empty database to make the tables with all fields.

### Configuration
Open `app/lib/models/Event.php` and `app/lib/models/Type.php` to set your database
access, at the beginning of each file. Improvement could be to set infos
in a config file.

### Email Configuration
You can be notified by mail if a new event is added.
Set your mail in `app/lib/models/Event.php`,  `l.182`.
You can skip this notification by adding you mail `l.125` of the same file.
