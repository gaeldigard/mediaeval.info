<?php
return [
    'sidebar.presentation' => 'Alla medeltida event i Europa',
    'sidebar.filters' => 'Filtrera',
    'sidebar.filters.keyword' => 'Nyckelord',
    'sidebar.filters.date' => 'Datum',
    'sidebar.filters.type' => 'Typ',
    'sidebar.filters.reset' => 'Återställ filter',
    'sidebar.settings' => 'Inställningar',
    'sidebar.settings.add' => 'Lägg till event',
    'sidebar.settings.contact' => 'Kontakt',
    // modals
    'modal.keyword.title' => 'Sök',
    'modal.keyword.placeholder' => 'Paris, Historia, XVe, Vikingar...',
    'modal.keyword.label' => 'Stadens namn, festens namn, eller ett tema',

    'modal.type.title' => 'Event typ',
    'modal.type.label' => 'Klicka på event-typen du vill ha',

    'modal.success.title' => 'Event tillagt!',
    'modal.success.content' => 'Ditt event har blivit tillagt i vår databas! <br/> Det kommer snart valideras av vårt team.<br/><br/> Tack! ',

    // add event
    'add.title' => 'Lägg till medeltida event',
    'add.desc' => 'Du kan lägga till ett event även om du inte organiserar det själv. Du kan lägga till tidigare event. Om event-typerna du söker inte finns kan du lägga till ett nytt i slutet, i beskrivnings-rutan.',
    'add.contact.title' => 'Kontakt',
    'add.contact.name' => 'Ditt för- och efternamn',
    'add.contact.email' => 'Din e-mail',
    'add.event.title' => 'Event-information',
    'add.event.name' => 'Namn : Paris medeltida fest',
    'add.event.address' => 'Event adress : Stadshuset, Blomgatan 3, etc.',
    'add.event.city' => 'Event stad',
    'add.event.country' => 'Land',
    'add.event.start_date' => 'Startdatum',
    'add.event.start_end' => 'Slutdatum',
    'add.event.website' => 'Hemsida',
    'add.event.type' => 'Event typ',
    'add.event.cost' => 'Pris',
    'add.event.cost.label' => 'Gratis, €2, €3.5 utan utklädnad...',
    'add.event.desc' => 'Komplett beskrivning av eventet, deltagare, animationer, schema, shower. Om det finns en turnering, vänligen specificera vilken sort : fäktning, duell...',

    // globals
    'cancel' => 'Avbryt',
    'save' => 'Spara',

    'pun' => 'You don\'t speak <a target="_blank" href="https://www.youtube.com/watch?v=cJx_QWaR3sw">Ikea</a> ?',


];
