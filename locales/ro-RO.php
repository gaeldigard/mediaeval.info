<?php
return [
'sidebar.presentation' => 'Toate evenimentele medievale din Europa',
  'sidebar.filters' => 'Filtru',
  'sidebar.filters.keyword' => 'Cuvant cheie',

  'sidebar.filters.date' => 'Data',
  'sidebar.filters.type' => 'Tip',
  'sidebar.filters.reset' => 'Reset filtru',
  'sidebar.settings' => 'Setari',
  'sidebar.settings.add' => 'Adauga eveniment',
  'sidebar.settings.contact' => 'Contact',
  // modals
  'modal.keyword.title' => 'Cauta',
  'modal.keyword.placeholder' => 'Paris  Istorie  al XV lea  Vikingi',
  'modal.keyword.label' => 'Oras ,petrecere, tema',

  'modal.type.title' => 'Tipul evenimentului',
  'modal.type.label' => 'Click pe evenimentul dorit',
 
  'modal.success.title' => 'Eveniment adaugat',
  'modal.success.content' => 'Evenimentul a fost adaugat in baza noastra de date<br/> Va fi in curand validat de echipa noastra. <br/><br/> Multumim',

  // add event
  'add.title' => 'Adauga un eveniment medieval',
  'add.desc' => 'Poti adauga un eveniment chiar daca nu il organizezi.  Poti adauga evenimente din trecut. daca tipul evenimentului nu se potriveste cu nevoile dumneavoastra, adaugati la final,in rubrica descriere',
  'add.contact.title' => 'Contact',
  'add.contact.name' => 'Nume si prenume',
  'add.contact.email' => 'E mail'
  'add.event.title' => 'Info eveniment'
  'add.event.name' => 'Nume'
  'add.event.address' => 'Adresa evenimentului',
  'add.event.city' => 'Orasul',
  'add.event.country' => 'Tara',
  'add.event.start_date' => 'Data inceperii'
  'add.event.start_end' => 'Data incheierii',
  'add.event.website' => 'Pagina',
  'add.event.type' => 'Tipul evenimentului',
  'add.event.cost' => 'Dauna'
  'add.event.cost.label' => 'Gratis, 3500, fara costum'
  'add.event.desc' => 'Completeaza descrierea evenimentui: participanti, atractii, program, spectacol'

  // globals
  'cancel' => 'Anuleaza',
  'save' => 'Salveaza',
  'pun' => 'You don\'t speak <a href="https://www.youtube.com/watch?v=jqFWDElfmWA" target="_blank">Dracula</a> ?'

];
