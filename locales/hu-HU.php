<?php
  return [
      'sidebar.presentation' => 'Középkori események Európában',
      'sidebar.filters' => 'Szűrők',
      'sidebar.filters.keyword' => 'Kulcsszavak',
      'sidebar.filters.date' => 'Dátum',
      'sidebar.filters.type' => 'Típus',
      'sidebar.filters.reset' => 'Szűrők törlése',
      'sidebar.settings' => 'Beállítások',
      'sidebar.settings.add' => 'Esemény hozzáadása',
      'sidebar.settings.contact' => 'Kapcsolat',
      // modals
      'modal.keyword.title' => 'Keresés',
      'modal.keyword.placeholder' => 'Párizs, Történelem, 15.sz, Vikingek...',
      'modal.keyword.label' => 'Írj be egy várost, társaságot vagy témát...',

      'modal.type.title' => 'Esemény típusa',
      'modal.type.label' => 'Kattints a keresett esemény típusra',

      'modal.success.title' => 'Esemény hozzáadva!',
      'modal.success.content' => 'Az eseményed sikeresen hozzáadtuk az adatbázisunkhoz! <br/>Csapatunk hamarosan ellenőrzi.<br/><br/> Köszönjük! ',

      // add event
      'add.title' => 'Középkori esemény hozzáadása',
      'add.desc' => 'Hozzáadhasz eseményt akkor is, ha nem te vagy a szervező. Hozzáadhatsz múltbeli eseményt is. Ha az esemény típusához nem találsz megfelelőt, a leírás mezőben add meg.',
      'add.contact.title' => 'Kapcsolat',
      'add.contact.name' => 'Keresztneved és vezetékneved',
      'add.contact.email' => 'E-mail címed',
      'add.event.title' => 'Információ az eseményről',
      'add.event.name' => 'Név: Párizsi Középkori Fesztivál',
      'add.event.address' => 'Esemény címe: utca, házszám',
      'add.event.city' => 'Esemény városa',
      'add.event.country' => 'Ország',
      'add.event.start_date' => 'Kezdede',
      'add.event.start_end' => 'Vége',
      'add.event.website' => 'Honlap',
      'add.event.type' => 'Esemény típus',
      'add.event.cost' => 'Költség',
      'add.event.cost.label' => 'Ingyenes, 2€, 3.5€ jelmez nélkül...',
      'add.event.desc' => 'Az esemény részletes leírása: résztvevők, program, előadások. Ha lovagi torna, milyen típusú...',

      // globals
      'cancel' => 'Mégse',
      'save' => 'Mentés',
      'pun' => 'You don\'t speak rubik\'s cube ? ', // I don't get this one :)


  ];
?>
