<?php
return [
    'sidebar.presentation' => 'Όλες οι Μεσαιωνικές εκδηλώσεις στην Ευρώπη ',
    'sidebar.filters' => 'Φιλτράρισμα με',
    'sidebar.filters.keyword' => 'Λέξη-κλειδί',
    'sidebar.filters.date' => 'Ημερομηνία',
    'sidebar.filters.type' => 'Τύπος',
    'sidebar.filters.reset' => 'Επαναφορά φίλτρων',
    'sidebar.settings' => 'Ρυθμίσεις',
    'sidebar.settings.add' => 'Προσθέστε μια εκδήλωση',
    'sidebar.settings.contact' => 'Επικοινωνία',
    // modals
    'modal.keyword.title' => 'Αναζήτηση',
    'modal.keyword.placeholder' => 'Παρίσι, Ιστορία, XV, Βίκινγκς ...',
    'modal.keyword.label' => 'Πληκτρολογήστε όνομα πόλης, όνομα εκδήλωσης ή ένα θέμα',

    'modal.type.title' => 'Τύπος εκδήλωσης',
    'modal.type.label' => 'Πατήστε στον τύπο εκδήλωσης που επιθυμείτε.',

    'modal.success.title' => 'Η εκδήλωση σας προστέθηκε με επιτυχία !',
    'modal.success.content' => 'Η εκδήλωση σας προστεθηκε με επιτυχία στη βάση δεδωμένων ! <br/> Παραμένει η επικύρωση της από την ομάδα μας <br/><br/> Σας ευχαριστούμε ! ',

    // add event
    'add.title' => 'Προσθέστε μια Μεσαιωνική εκδήλωση',
    'add.desc' => 'Μπορείτε να προσθεσετε μια εκδήλωση ακόμα και αν δεν είστε ο οργανωτής της. Μπορείτε να προσθέσετε παλαιότερες εκδηλώσεις. Αν οι τύποι εκδηλώσεων δεν ανταποκρίνονται στις ανάγκες σας, μπορείτε να προσθέσετε τις ανάγκες σας στο τέλος, στο κείμενο περιγραφής.',
    'add.contact.title' => 'Προσωπικά στοιχεία',
    'add.contact.name' => 'Ονοματεπώνυμο',
    'add.contact.email' => 'Το e-mail σας',
    'add.event.title' => 'Πληροφορίες εκδήλωσης',
    'add.event.name' => 'Όνομα: Μεσαιωνικό φεστιβάλ Παρισίου',
    'add.event.address' => 'Διευθυνσή εκδήλωσης: city hall, street flowers 3, etc',
    'add.event.city' => 'Πόλη εκδήλωσης',
    'add.event.country' => 'Χώρα',
    'add.event.start_date' => 'Ημερομηνία έναρξης',
    'add.event.start_end' => 'Ημερομηνία λήξης',
    'add.event.website' => 'Ιστοσελίδα',
    'add.event.type' => 'Τύπος εκδήλωσης',
    'add.event.cost' => 'Κόστος',
    'add.event.cost.label' => 'Δωρεάν, 2€, 3.5€ εαν δεν υπάρχει αμφίεση...',
    'add.event.desc' => 'Συμπληρώστε την πειγραφη της εκδήλωσης : Συμμετέχωντες , πρόγραμμα, σόου… Εάν πρόκειται για τουρνουά, παρακαλούμε να διευκρινίσετε τι είδους: joust, behourd...',

    // globals
    'cancel' => 'Ακύρωση',
    'save' => 'Αποθήκευση',
    
    'pun' => 'You don\'t speak Zeus ? ',

  ];
