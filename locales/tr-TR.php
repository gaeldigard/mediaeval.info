<?php
return [
    'sidebar.presentation' => 'Avrupa\'daki tüm ortaçağ etkinlikleri',
    'sidebar.filters' => 'Filtrele',
    'sidebar.filters.keyword' => 'Anahtar Kelimeler',
    'sidebar.filters.date' => 'Tarih',
    'sidebar.filters.type' => 'Tip',
    'sidebar.filters.reset' => 'Filtreleri sıfırla',
    'sidebar.settings' => 'Ayarlar',
    'sidebar.settings.add' => 'Etkinlik Ekle',
    'sidebar.settings.contact' => 'İletişim',
    // modals
    'modal.keyword.title' => 'Ara',
    'modal.keyword.placeholder' => 'Paris, Tarih, XV, Vikingler...',
    'modal.keyword.label' => 'Şehir, parti adı veya bir tema yazın',

    'modal.type.title' => 'Etkinlik tipi',
    'modal.type.label' => 'İstediğiniz etkinlik türünü tıklayın',

    'modal.success.title' => 'Etkinlik eklendi!',
    'modal.success.content' => 'Etkinliğiniz veritabanımıza başarıyla eklendi! <br/> Moderatörler tarafından kısa süre içinde onaylanacak. <br/> <br/> Teşekkür ederiz! ',

    // add event
    'add.title' => 'Bir ortaçağ etkinliği ekle',
    'add.desc' => 'Organize etmeseniz bile etkinlik ekleyebilirsiniz. Geçmiş etkinlikleri ekleyebilirsiniz. Etkinlik türleri ihtiyaçlarınıza uygun değilse, sonuna, açıklama metin kutusuna ekleyebilirsiniz. ',
    'add.contact.title' => 'İletişim',
    'add.contact.name' => 'Adınız ve soyadınız',
    'add.contact.email' => 'E-posta adresiniz',
    'add.event.title' => 'Etkinlik bilgileri',
    'add.event.name' => 'Adı: Paris in Ortaçağ Şöleni',
    'add.event.address' => 'Etkinlik adresi : belediye binası, sokak çiçekleri 3, vb.',
    'add.event.city' => 'Etkinlik şehri',
    'add.event.country' => 'Ülke',
    'add.event.start_date' => 'Başlama tarihi',
    'add.event.start_end' => 'Bitiş tarihi',
    'add.event.website' => 'Website',
    'add.event.type' => 'Etkinlik tipi',
    'add.event.cost' => 'Ücret',
    'add.event.cost.label' => 'Ücretsiz, 2€, 3.5€ kostüm yoksa...',
    'add.event.desc' => 'Etkinliğin tam açıklaması: katılımcılar, animasyonlar, zamanlama, şovlar. Eğer bir turnuva ise, lütfen ne tür olduğunu belirtin: şövalye, asker ...',

    // globals
    'cancel' => 'İptal',
    'save' => 'Kaydet',

    'pun' => 'You don\'t speak loukoum ?',


  ];
