<?php

return [
  'sidebar.presentation' => 'Tutti gli eventi medioevali in Europa',

    'sidebar.filters' => 'Filtra per',

    'sidebar.filters.keyword' => 'Parola chiave',

    'sidebar.filters.date' => 'Data',

    'sidebar.filters.type' => 'Tipo',

    'sidebar.filters.reset' => 'Azzera filtri',

    'sidebar.settings' => 'Settaggi',

    'sidebar.settings.add' => ' Aggiungi un evento',

    'sidebar.settings.contact' => 'Contatti',

    // modals

    'modal.keyword.title' => 'Cerca',

    'modal.keyword.placeholder' => 'Parigi, storia, XV, Vichinghi…',

    'modal.keyword.label' => 'Digita il nome di una città, di una festa o di un tema',

    'modal.type.title' => 'Tipo di evento',

    'modal.type.label' => 'Clicca sul tipo di evento che desideri',

    'modal.success.title' => 'Evento aggiunto!',

    'modal.success.content' => ' Il tuo evento è stato aggiunto correttamente nel nostro database!<br/> Sarà validato presto dal nostro team. <br/><br/> Grazie!',

    // add event

    'add.title' => 'Aggiungi un evento medioevale',

    'add.desc' => 'Puoi aggiungere un evento, anche se non lo organizzi. Puoi aggiungere eventi passati. Se i tipi di evento presenti non corrispondono a quanto stai cercando, puoi aggiungere il tuo alla fine, nella casella di testo.',

    'add.contact.title' => ' Contatti',

    'add.contact.name' => ' Il tuo nome e cognome',

    'add.contact.email' => 'La tua email',

    'add.event.title' => 'Informazioni sull’evento',

    'add.event.name' => ' Nome: festa medioevale di Parigi ',

    'add.event.address' => ' Indirizzo dell’evento: municipio, via dei fiori 3, ecc.',

    'add.event.city' => 'Città dell’evento',

    'add.event.country' => 'Nazione',

    'add.event.start_date' => 'Data inizio',

    'add.event.start_end' => 'Data fine',

    'add.event.website' => 'Sito internet',

    'add.event.type' => 'Tipo di evento',

    'add.event.cost' => 'Prezzo',

    'add.event.cost.label' => 'Gratis, 2€, 3,5€ senza costume',

    'add.event.desc' => 'Completa la descrizione dell’evento: partecipanti, animazioni, programma, spettacoli. In caso di torneo, per favore, specifica di che tipo: giostra…',

    // globals

    'cancel' => 'Cancella',

    'save' => 'Salva',

    'pun' => 'You don\'t speak mamamia ?'
];

?>
