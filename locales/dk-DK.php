<?php
return [
    'sidebar.presentation' => 'Alle middelalder events I Europa',
    'sidebar.filters' => 'Filtrér via',
    'sidebar.filters.keyword' => 'Nøgleord',
    'sidebar.filters.date' => 'Dato',
    'sidebar.filters.type' => 'Type',
    'sidebar.filters.reset' => 'Nulstil filtre',
    'sidebar.settings' => 'Indstillinger',
    'sidebar.settings.add' => 'Tilføj event',
    'sidebar.settings.contact' => 'Kontakt',
    // modals
    'modal.keyword.title' => 'Søg',
    'modal.keyword.placeholder' => 'Paris, Historie, XVth Vikingere',
    'modal.keyword.label' => 'Skriv et bynavn, parti navn eller et tema',

    'modal.type.title' => 'Event type',
    'modal.type.label' => 'Klick på typen af event du ønsker',

    'modal.success.title' => 'Event tilføjet',
    'modal.success.content' => 'Dit event blev tilføjet vores database! <br/> Det bliver snart bekræftet af vores team. <br/><br/> Tak!',

    // add event
    'add.title' => 'Tilføj et middelalder event',
    'add.desc' => 'Du kan tilføje et event, selv om du ikke arrangerer det. Du kan tilføje tidligere events. Hvis event typerne ikke matcher dine behov, så kan du tilføje dit event for enden og beskrive det i testfeltet.',
    'add.contact.title' => 'Kontakt',
    'add.contact.name' => 'Dit for- og efternavn',
    'add.contact.email' => 'Din e-mail adresse',
    'add.event.title' => 'Event informationer',
    'add.event.name' => 'Navn: Paris middelalder fest',
    'add.event.address' => 'Evenet adresse',
    'add.event.city' => 'Event by',
    'add.event.country' => 'Land',
    'add.event.start_date' => 'Start dato',
    'add.event.start_end' => 'Slut dato',
    'add.event.website' => 'Hjemmeside',
    'add.event.type' => 'Event type',
    'add.event.cost' => 'Pris',
    'add.event.cost.label' => 'Gratis, 2€, ...',
    'add.event.desc' => 'Komplet beskrivelse af eventet: Deltagere, animationer, skema, forestillinger. Hvis det er en kappestrid, så angiv venligst hvilken slags.',
    // globals
    'cancel' => 'Slet',
    'save' => 'Gem',
    'pun' => 'You don\'t speak Haraldr Gormsson ? ',


];
