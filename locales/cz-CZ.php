<?php
return [
    'sidebar.presentation' => 'Všechny středověké události v Evropě',
    'sidebar.filters' => 'Filtruj dle',
    'sidebar.filters.keyword' => 'Klíčové slovo',
    'sidebar.filters.date' => 'Datum',
    'sidebar.filters.type' => 'Typ',
    'sidebar.filters.reset' => 'Vymazat filtry',
    'sidebar.settings' => 'Nastavení',
    'sidebar.settings.add' => 'Přidej událost',
    'sidebar.settings.contact' => 'Kontakt',
    // modals
    'modal.keyword.title' => 'Hledej',
    'modal.keyword.placeholder' => 'Praha, Historie, XVth, Vikingové...',
    'modal.keyword.label' => 'Vyplň název města, název události nebo její téma',

    'modal.type.title' => 'Typ události',
    'modal.type.label' => 'Klikni na typ události, který chceš',

    'modal.success.title' => 'Událost přidána !',
    'modal.success.content' => 'Vaše událost byla úspěšně přidána do naší databáze! <br/> Brzy bude potvrzena naším týmem.<br/><br/> Díky! ',

    // add event
    'add.title' => 'Typ středověké události',
    'add.desc' => 'Můžete přidat událost, i když nejste organizátor. Můžete přidávat i minulé události. Pokud předefinované typy událostí neodpovídají té vaší, můžete si přidat vlastní typ do pole popisu na konci.',
    'add.contact.title' => 'Kontakt',
    'add.contact.name' => 'Vaše jméno a příjmení',
    'add.contact.email' => 'Váš email',
    'add.event.title' => 'Informace o události',
    'add.event.name' => 'Name : Pražský středověký festival',
    'add.event.address' => 'Adresa události: Radnice, Liliová 8, atd',
    'add.event.city' => 'Město konání události',
    'add.event.country' => 'Stát',
    'add.event.start_date' => 'Datum zahájení',
    'add.event.start_end' => 'Datum ukončení',

    'add.event.website' => 'Webová stránka',
    'add.event.type' => 'Typ události',
    'add.event.cost' => 'Cena',
    'add.event.cost.label' => 'Zdarma, 2€, 3.5€ pokud jste bez kostýmu...',
    'add.event.desc' => 'Kompletní popis události: účastníci, harmonogram a program akce, show. Pokud je to nějaká soutěž/turnaj, ´prosím specifikujte jaký: klání...',

    // globals
    'cancel' => 'Zrušit',
    'save' => 'Uložit',

    'pun' => 'You don\'t speak becherovka ? ',


];
