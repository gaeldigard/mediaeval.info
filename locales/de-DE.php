<?php
return [
    'sidebar.presentation' => 'Alle Mittelalterveranstaltungen in Europa',
    'sidebar.filters' => 'Filtern mit',
    'sidebar.filters.keyword' => 'Keyword',
    'sidebar.filters.date' => 'Datum',
    'sidebar.filters.type' => 'Typ',
    'sidebar.filters.reset' => 'Filter zurücksetzen',
    'sidebar.settings' => 'Einstellungen',
    'sidebar.settings.add' => 'Veranstaltung hinzufügen',
    'sidebar.settings.contact' => 'Kontakt',
    // modals
    'modal.keyword.title' => 'Suche',
    'modal.keyword.placeholder' => 'Paris, Geschichte, Wikinger',
    'modal.keyword.label' => 'Tippe einen Stadtnamen, Partynamen oder ein Motto',

    'modal.type.title' => 'Veranstaltungstyp',
    'modal.type.label' => 'Klicke auf den Veranstaltungstyp deiner Wahl',

    'modal.success.title' => 'Veranstaltung hinzugefügt',
    'modal.success.content' => 'Deine Veranstaltung wurde erfolgreich unserer Datenbank hinzugefügt! <br/> Es wird alsbald von unserem Team validiert <br/><br/> Danke sehr!',

    // add event
    'add.title' => 'Füge eine Mittelalterveranstaltung hinzu',
    'add.desc' => 'Du kannst eine Veranstaltung hinzufügen, auch wenn du diese nicht organisierst. Du kannst vergangene Veranstaltungen hinzufügen. Auch wenn die Veranstaltungenstypen nicht passen, kannst du eigene am Ende eingeben.',
    'add.contact.title' => 'Kontakt',
    'add.contact.name' => 'Dein Vor- und Nachname',
    'add.contact.email' => 'Deine e-mail Adresse',
    'add.event.title' => 'Veranstaltungsinformationen',
    'add.event.name' => 'Name : Mittelalterfestival von Paris',
    'add.event.address' => 'Eventadresse : Stadthalle, Blumenstraße 3 usw.',
    'add.event.city' => 'Veranstaltungsort',
    'add.event.country' => 'Land',
    'add.event.start_date' => 'Beginn',
    'add.event.start_end' => 'Ende',
    'add.event.website' => 'Webseite',
    'add.event.type' => 'Veranstaltungstyp',
    'add.event.cost' => 'Kosten',
    'add.event.cost.label' => 'Umsonst, 2€, 3.5€ wenn ohne Kostüm',
    'add.event.desc' => 'Komplette Beschreibung der Veranstaltung: Teilnehmer, Ablauf, Shows. Falls du ein Turnier einträgst, spezifiziere die Art: Rittturnier, Behoust usw.',
    // globals
    'cancel' => 'Abbrechen',
    'save' => 'Speichern',
    'pun' => 'You don\'t speak strudel ? ',


];
