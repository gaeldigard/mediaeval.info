<?php
return [
    'sidebar.presentation' => 'Все средневековые события в Европе',
    'sidebar.filters' => 'Сортировать по',
    'sidebar.filters.keyword' => 'Ключевое слово',
    'sidebar.filters.date' => 'Дата',
    'sidebar.filters.type' => 'Тип',
    'sidebar.filters.reset' => 'Сбросить фильтры',
    'sidebar.settings' => 'Настройки',
    'sidebar.settings.add' => 'Добавить событие',
    'sidebar.settings.contact' => 'Контакт',
    // modals
    'modal.keyword.title' => 'Поиск',
    'modal.keyword.placeholder' => 'Париж, История, XV, Викинги...',
    'modal.keyword.label' => 'Введите название города, название события или тему',

    'modal.type.title' => 'Тип события',
    'modal.type.label' => 'Нажмите на тип события, который вы хотите',

    'modal.success.title' => 'Событие добавлено !',
    'modal.success.content' => 'Ваше мероприятие было успешно добавлено в нашу базу данных ! <br/> Оно будет скоро подтверждено нашей командой.<br/><br/> Спасибо ! ',

    // add event
    'add.title' => 'Добавить средневековое событие',
    'add.desc' => 'Вы можете добавить событие, даже если вы не его организатор. Вы можете добавить прошедшие события. Если типы событий не соответствуют вашим потребностям, вы можете добавить свои в конце, в текстовом поле описания.',
    'add.contact.title' => 'Контакт',
    'add.contact.name' => 'Ваше имя и фамилия',
    'add.contact.email' => 'Ваш адрес электронной почты',
    'add.event.title' => 'Информация о событии',
    'add.event.name' => 'Название: Средневековый фестиваль Парижа',
    'add.event.address' => 'Адрес мероприятия: мэрия, улица цветочная 3 и т. д.',
    'add.event.city' => 'Город проведения события',
    'add.event.country' => 'Страна',
    'add.event.start_date' => 'Дата начала',
    'add.event.start_end' => 'Дата окончания',
    'add.event.website' => 'Веб-сайт',
    'add.event.type' => 'Тип события',
    'add.event.cost' => 'Цена',
    'add.event.cost.label' => 'Бесплатно, 2€, 3.5€ если нет костюма...',
    'add.event.desc' => 'Полное описание мероприятия: участники, анимация, расписание, шоу. Если это турнир, пожалуйста, укажите, какой тип: joust, behourd...',

    // globals
    'cancel' => 'Отменить',
    'save' => 'Сохранить',

    'pun' => 'You don\'t speak <a target="_blank" href="https://www.youtube.com/watch?v=bo5ZVe1LHxU">vodka</a> ? ',


];
