<?php
return [
    'sidebar.presentation' => 'All medieval events in Europe',
    'sidebar.filters' => 'Filter by',
    'sidebar.filters.keyword' => 'Keyword',
    'sidebar.filters.date' => 'Date',
    'sidebar.filters.type' => 'Type',
    'sidebar.filters.reset' => 'Reset filters',
    'sidebar.settings' => 'Settings',
    'sidebar.settings.add' => 'Add an event',
    'sidebar.settings.contact' => 'Contact',
    // modals
    'modal.keyword.title' => 'Search',
    'modal.keyword.placeholder' => 'Paris, History, XVth, Vikings...',
    'modal.keyword.label' => 'Type a city name, party name, or a theme',

    'modal.type.title' => 'Event type',
    'modal.type.label' => 'Click on the event type you want',

    'modal.success.title' => 'Event added !',
    'modal.success.content' => 'Your event was added successfully in our database ! <br/> It will be soon validated by our team.<br/><br/> Thank you ! ',

    // add event
    'add.title' => 'Add a medieval event',
    'add.desc' => 'You can add an event, even if you don\'t organize it. You can add past events. If the events types don\'t match your needs, you can add yours at the end, in the description textbox.',
    'add.contact.title' => 'Contact',
    'add.contact.name' => 'Your first and last name',
    'add.contact.email' => 'Your e-mail',
    'add.event.title' => 'Event informations',
    'add.event.name' => 'Name : Medieval fest of Paris',
    'add.event.address' => 'Event address : city hall, street flowers 3, etc',
    'add.event.city' => 'Event city',
    'add.event.country' => 'Country',
    'add.event.start_date' => 'Start date',
    'add.event.start_end' => 'End date',
    'add.event.website' => 'Website',
    'add.event.type' => 'Event type',
    'add.event.cost' => 'Cost',
    'add.event.cost.label' => 'Free, 2€, 3.5€ if no costume...',
    'add.event.desc' => 'Complete description of the event : participants, animations, schedule, shows. If it\'s a tournament, please specify what kind : joust, behourd...',

    // globals
    'cancel' => 'Cancel',
    'save' => 'Save',
    'pun' => 'You don\'t speak rosbeef ? ',


];
