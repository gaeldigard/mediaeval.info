<?php
return [
    'sidebar.presentation' => ' كل أحداث القرون الوسطى في أوروبا ',
    'sidebar.filters' => 'مُصفى ب ',
    'sidebar.filters.keyword' => 'كلمة دلالية ',
    'sidebar.filters.date' => 'تاريخ ',
    'sidebar.filters.type' => ' نوع ',
    'sidebar.filters.reset' => ' إعادة ضبط المُنقي  ',
    'sidebar.settings' => ' إعدادات ',
    'sidebar.settings.add' => 'إضافة حدث ',
    'sidebar.settings.contact' => 'جهة اتصال',
    // modals =>  اتصال
    'modal.keyword.title' => ' بحث ',
    'modal.keyword.placeholder' => ' باريس, تاريخ ، الخامس عشر ، الفايكينج     ',
    'modal.keyword.label' => ' اكتب اسم مدينة ، حفل ، اسم ، أو ',

    'modal.type.title' => 'نوع الحدث ',
    'modal.type.label' => 'انقر على نوع الحدث الذي تريده',

    'modal.success.title' => ' تمت إضافة الحدث !',
    'modal.success.content' => ' تمت إضافة الحدث الخاص بنجاح على قاعدة البيانات ، و سيقوم فريقنا بمراجعته قريباً   ',
 
    // add event
    'add.title' => ' أضف حدثا من القرون الوسطى ',
    'add.desc' => ' بإمكان إضافة حدث حتى لو لم تكن منظماً له ، يمكنك إضافة حدثك الخاص في النهاية  ',
    'add.contact.title' => ' جهة اتصال    ',
    'add.contact.name' => ' الاسم الأول و اسم العائلة  ',
    'add.contact.email' => ' بريدك الإلكتروني  ',
    'add.event.title' => ' معلومات الحدث ',
    'add.event.name' => ' الإسم :مهرجان من القرون الوسطى في باريس ',
    'add.event.address' => ' عنوان الحدث :قاعة المدينة ',
    'add.event.city' => ' مدينة الحدث',
    'add.event.country' => '  بلد الحدث ',
    'add.event.start_date' => ' يبدأ في تاريخ ',
    'add.event.start_end' => ' ينتهي في تاريخ ',
    'add.event.website' => ' الموقع الإلكتروني ',
    'add.event.type' => '  نوع الحدث ',
    'add.event.cost' => '  التكلفة ',
    'add.event.cost.label' => '  فئات التذاكر ، 2، 3.5 .. ',
    'add.event.desc' => ' وصف كامل للحدث : المشاركين ، نوع العرض ، الجدول الزمني ، إلى العروض . إذا كانت مسابقة  ، الرجاء تحديد نوع المسابقة : منافسة ، مناظرة ....',

    // globals
    'cancel' => ' إلغاء ',
    'save' => 'حفظ ',

    'pun' => 'You don\'t speak Aladdin ?'

  ];
