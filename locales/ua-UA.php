<?php
return [
    'sidebar.presentation' => 'Усі середньовічні події в Європі',
    'sidebar.filters' => 'Фільтрувати за',
    'sidebar.filters.keyword' => 'Ключове слово',

    'sidebar.filters.date' => 'Дата',
    'sidebar.filters.type' => 'Тип',

    'sidebar.filters.reset' => 'Скинути фільтри',
    'sidebar.settings' => 'Налаштування',
    'sidebar.settings.add' => 'Додати подію',
    'sidebar.settings.contact' => 'Контакт',
    // modals
    'modal.keyword.title' => 'Пошук',
    'modal.keyword.placeholder' => 'Париж, історія XV, вікінги...',
    'modal.keyword.label' => 'Введіть назву міста, назву події або тему',

    'modal.type.title' => 'Тип події',
    'modal.type.label' => 'Натисніть на потрібний тип події',

    'modal.success.title' => 'Подія додана !',
    'modal.success.content' => 'Ваша подія успішно додана в нашу базу даних ! <br/> Вона скоро буде підтверджена нашою командою.<br/><br/> Дякую ! ',

    // add event
    'add.title' => 'Додайте середньовічну подію',
    'add.desc' => 'Ви можете додати подію, навіть якщо не являєтесь її організатором. Ви можете додавати минулі події. Якщо типи подій не відповідають вашим потребам, ви можете додати потрібний вам тип у кінці, в текстовому полі опису.',
    'add.contact.title' => 'Контакти',
    'add.contact.name' => 'Ваше ім\'я та прізвище',
    'add.contact.email' => 'Ваша електронна адреса',
    'add.event.title' => 'Інформація про подію',
    'add.event.name' => 'Назва: Середньовічний фестиваль Парижа',
    'add.event.address' => 'Адреса події: мерія така-то, вулиця квіткова 3 тощо',
    'add.event.city' => 'Місто де проводиться подія',
    'add.event.country' => 'Країна',
    'add.event.start_date' => 'Дата початку',
    'add.event.start_end' => 'Дата кінця',
    'add.event.website' => 'Веб-сайт',
    'add.event.type' => 'Тип події',
    'add.event.cost' => 'Вартість',
    'add.event.cost.label' => 'Безкоштовно, 2€, 3.5€ у разі відсутності костюму...',
    'add.event.desc' => 'Повний опис події: учасники, анімація, розклад, шоу. Якщо це турнір, будь-ласка, вкажіть, який вид: joust, behourd...',

    // globals
    'cancel' => 'Скасувати',
    'save' => 'Зберегти',

    'pun' => 'You don\'t speak chernobyl ? ',


];
