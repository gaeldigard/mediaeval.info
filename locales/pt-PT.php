<?php
return [
    'sidebar.presentation' =>  "Todos os eventos medievais na Europa",
    'sidebar.filters' =>  "Filtrar por",
    'sidebar.filters.keyword' =>  "Palavra-chave",
    'sidebar.filters.date' =>  "Data",
    'sidebar.filters.type' => "Tipo",
    'sidebar.filters.reset' =>  "Apagar filtros",
    'sidebar.settings' => "Configurações",
    'sidebar.settings.add' => "Adicione um evento",
    'sidebar.settings.contact' => "Contacto",
    // modals
    'modal.keyword.title' => "Procurar",
    'modal.keyword.placeholder' => "Paris, História, Século XV, Viquingues",
    'modal.keyword.label' => "Escreva o nome de uma cidade, nome de uma festa, ou um tema",

    'modal.type.title' => "Tipo de evento",
    'modal.type.label' => "Clique no tipo de evento que pretende",

    'modal.success.title' =>  "Evento adicionado!",
    'modal.success.content' => "O seu evento foi adicionado com sucesso à nossa base de dados! <br/> Em breve será validado pela nossa equipa. <br/> Obrigado!",

    // add event
    'add.title' => "Adicione um evento medieval",
    'add.desc' => "Pode adicionar um evento mesmo não sendo o organizador. Pode adicionar eventos passados. Se os tipos de evento não satisfazem as suas necessidades, pode adicionar o seu tipo de evento no final usando a caixa para descrição.",
    'add.contact.title' => "Contacto",
    'add.contact.name' => "O seu primeiro e último nome",
    'add.contact.email' => "O seu e-mail",
    'add.event.title' => "Informações de evento",
    'add.event.name' => "Nome: Feira Medieval de Paris",
    'add.event.address' =>  "Endereço do evento: Câmara Municipal, Rua das Flores 3, etc",
    'add.event.city' => "Cidade do evento",
    'add.event.country' =>  "País",
    'add.event.start_date' => "Início do evento",
    'add.event.start_end' => "Fim do evento",
    'add.event.website' => 'Website',
    'add.event.type' => "Tipo de evento",
    'add.event.cost' => "Preço",
    'add.event.cost.label' => "Grátis, 2€, 3,5€ sem traje",
    'add.event.desc' =>  "Descrição completa do evento: participantes, animações, horários, espectáculos. Se se tratar de um torneio, por favor especifique de que tipo: justa, justa entre cavaleiros, behourd..",

    // globals
    'cancel' =>  "Cancelar",
    'save' =>  "Guardar",
    'pun' =>  "You don't speak Manuel ?"


];
