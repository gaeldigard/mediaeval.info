<?php
return [
    'sidebar.presentation' => 'Todos los eventos medievales en Europa',
    'sidebar.filters' => 'Filtrado por',
    'sidebar.filters.keyword' => 'Palabra clave',
    'sidebar.filters.date' => 'Fecha',
    'sidebar.filters.type' => 'Tipo',
    'sidebar.filters.reset' => 'Resetear filtros',
    'sidebar.settings' => 'Ajustes',
    'sidebar.settings.add' => 'Agregar un evento',
    'sidebar.settings.contact' => 'Contacto',
    // modals
    'modal.keyword.title' => 'Buscar',
    'modal.keyword.placeholder' => 'Paris, historia, XV, vikingos',

    'modal.keyword.label' => 'Escribe un nombre de ciudad, fiesta o tema',
    'modal.type.title' => 'Tipo de evento',
    'modal.type.label' => 'Click en el tipo de evento que deseas',

    'modal.success.title' => 'Evento añadido',
    'modal.success.content' => 'Tu evento ha sido añadido exitosamente en nuestra base de datos<br/> Pronto será validado por nuestro equipo <br/><br/>Gracias ! ',

    // add event
    'add.title' => 'Agregar un evento medieval',
    'add.desc' => 'Puedes añadir un evento, incluso si no lo organizas. Puedes añadir eventos pasados. Si el tipo de evento la no coincide con lo que necesitas, puedes añadir los tuyos al final en la descripción del cuadro de texto.',
    'add.contact.title' => 'Contacto',
    'add.contact.name' => 'Nombre y apellido',
    'add.contact.email' => 'Tu correo electrónico',
    'add.event.title' => 'Información de eventos',
    'add.event.name' => 'Nombre: festival medieval en Barcelona',
    'add.event.address' => 'Dirección del evento',
    'add.event.city' => 'Ciudad del evento',
    'add.event.country' => 'País',
    'add.event.start_date' => 'Fecha de inicio',
    'add.event.start_end' => 'Fecha de termino',
    'add.event.website' => 'Página web',
    'add.event.type' => 'Tipo de evento',
    'add.event.cost' => 'Costo',
    'add.event.cost.label' => 'Gratis, 2€, 3.5€ si no hay disfraz',
    'add.event.desc' => 'Completa descripción del evento: participantes, animaciones, horario, shows..',

    // globals
    'cancel' => 'Cancelar',
    'save' => 'Guardar',
    'pun' => 'You don\'t speak <a target="_blank" href="https://www.youtube.com/watch?v=PHCt2uHVe58">salsa tequila corazon</a> ',


];
