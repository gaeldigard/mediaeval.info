<?php
return [
    'sidebar.presentation' => 'Tous les événements médiévaux en Europe.',
    'sidebar.filters' => 'Filtrer par',
    'sidebar.filters.keyword' => 'Mot-clef',
    'sidebar.filters.date' => 'Date',
    'sidebar.filters.type' => 'Type',
    'sidebar.filters.reset' => 'Effacer les filtres',
    'sidebar.settings' => 'Paramètres',
    'sidebar.settings.add' => 'Ajoutez un événement',
    'sidebar.settings.contact' => 'Contact',
    // modals
    'modal.keyword.title' => 'Recherche',
    'modal.keyword.placeholder' => 'Dinan, XVème, Histoire, Viking...',
    'modal.keyword.label' => 'Tapez un nom de ville, de fête ou de thème',

    'modal.type.title' => 'Type d\'événement',
    'modal.type.label' => 'Cliquez sur le type d\'événement désiré',

    'modal.success.title' => 'Évévement ajouté !',
    'modal.success.content' => 'Votre événement a été ajouté avec succès à notre base de données !<br/>Il est en cours de validation par notre équipe.<br/><br/>Merci !',

    // add event
    'add.title' => 'Ajoutez un événement médiéval',
    'add.desc' => 'Vous pouvez ajouter un événement même si vous n\'en êtes pas l\'organisateur. Vous pouvez ajouter des événements passés. Si les types d\'événements proposés ne vous conviennent pas, ajoutez votre propre type dans la description de l\'événement, au début.',
    'add.contact.title' => 'Contact',
    'add.contact.name' => 'Votre nom & prénom',
    'add.contact.email' => 'Votre e-mail',
    'add.event.title' => 'Informations de l\'événement',
    'add.event.name' => 'Nom : fête des remparts',
    'add.event.address' => 'Adresse de l\'événement : place du centre, 3 rue des fleurs, etc',
    'add.event.city' => 'Ville de l\'événement',
    'add.event.country' => 'Pays',
    'add.event.start_date' => 'Date de début',
    'add.event.start_end' => 'Date de fin',
    'add.event.website' => 'Site web',
    'add.event.type' => 'Type d\'événement',
    'add.event.cost' => 'Tarif',
    'add.event.cost.label' => 'Gratuit, 2€, 3.5€ si non costumé...',
    'add.event.desc' => 'Description complète de l\'événement : participants, animations,
    horaires, spectacles. S\'il s\'agit d\'un tournoi, précisez le genre : béhourd, joute... Soyez exhaustifs !',
    // globals
    'cancel' => 'Annuler',
    'save' => 'Enregistrer',
    'pun' => 'You don\'t speak <a target="_blank" href="https://www.youtube.com/watch?v=ERD2TnMNH98">baguette</a> ? ',

 ];
