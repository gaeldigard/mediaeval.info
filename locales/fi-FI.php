<?php
return [
    'sidebar.presentation' => 'Euroopan kaikki keskiaikaiset tapahtumat',
    'sidebar.filters' => 'Järjestä',
    'sidebar.filters.keyword' => 'Avainsanan mukaan',
    'sidebar.filters.date' => 'Päivämäärän mukaan',
    'sidebar.filters.type' => 'Kategorian mukaan',
    'sidebar.filters.reset' => 'Nollaa hakuperusteet',
    'sidebar.settings' => 'Asetukset',
    'sidebar.settings.add' => 'Lisää tapahtuma',
    'sidebar.settings.contact' => 'Ota yhteyttä',
    // modals
    'modal.keyword.title' => 'Hae',
    'modal.keyword.placeholder' => 'Pariisi, Historia, 1400-luku, Viikingit',
    'modal.keyword.label' => 'Kirjoita paikan nimi, osapuoli tai aihepiiri',

    'modal.type.title' => 'Tapahtuman tyyppi',
    'modal.type.label' => 'Valitse haluamasi tapahtuman tyyppi',

    'modal.success.title' => 'Tapahtuma lisätty',
    'modal.success.content' => 'Tapahtumasi lisättiin onnistuneesti tietokantaamme.<br/> Tarkastamme tallentamasi tiedot mahdollisimman<br/><br/> Nopeasti!',

    // add event
    'add.title' => 'Lisää keskiaikainen tapahtuma',
    'add.desc' => 'Voit lisätä haluamasi historiallisen tapahtuman tietokantaamme, vaikka et itse toimisi sen järjestäjänä. Voit lisätä tietokantaan myös jo päättyneitä tapahtumia. Jos saatavilla olevat tapahtumakategoriat eivät vastaa tarpeitasi, voit lisätä omasi lomakkeen lopussa olevaan “tapahtuman kuvaus” osioon.',
    'add.contact.title' => 'Yhteystietosi',
    'add.contact.name' => 'Etu- ja sukunimi',
    'add.contact.email' => 'Sähköpostiosoite',
    'add.event.title' => 'Tapahtuman tiedot',
    'add.event.name' => 'Nimi: Pariisin keskiaikaiset pidot',
    'add.event.address' => 'Tapahtuman osoite: Kaupungintalo, Kukkatie 3, jne.',
    'add.event.city' => 'Kaupunki',
    'add.event.country' => 'Maa',
    'add.event.start_date' => 'Alkamispäivämäärä',
    'add.event.start_end' => 'Päättymispäivämäärä',
    'add.event.website' => 'Internetosoite',
    'add.event.type' => 'Tapahtuman tyyppi',
    'add.event.cost' => 'Hinta',
    'add.event.cost.label' => 'Ilmainen, €2, €3.5 ilman asustetta',
    'add.event.desc' => 'Viimeistele tapahtuman kuvaus: osallistujat, roolipelit, aikataulu, esitykset. Jos kyseessä on turnaus, määrittele turnauksen tyyppi.',

    // globals
    'cancel' => 'Peruuta',
    'save' => 'Tallenna',

    'pun' => 'You don\'t speak <a href="https://youtu.be/4om1rQKPijI" target="_blank">Finntroll</a> ?'

];
