<?php
return [
    'sidebar.presentation' => 'Všetky stredoveké udalosti v Európe',
    'sidebar.filters' => 'Flitrovať',
    'sidebar.filters.keyword' => 'Kľúčové slovo',
    'sidebar.filters.date' => 'Dátum',
    'sidebar.filters.type' => 'Typ',
    'sidebar.filters.reset' => 'Resetovať filtre',
    'sidebar.settings' => 'Nastavenia',
    'sidebar.settings.add' => 'Pridať udalosť',
    'sidebar.settings.contact' => 'Kontakt',
    // modals
    'modal.keyword.title' => 'Hladať',
    'modal.keyword.placeholder' => 'Paríž, História, XVth , Vikingovia...',
    'modal.keyword.label' => ' Napísať názov mesta, názov skupiny alebo témy.',

    'modal.type.title' => 'Druh udalosti',
    'modal.type.label' => 'Kliknúť na požadovanú udalosť',

    'modal.success.title' => 'Udalosť pridaná !',
    'modal.success.content' => 'Vaša udalosť bola úspešne pridaná do databázy ! <br/> Čoskoro bude potvrdená našim týmom..<br/><br/> Ďakujeme Vám ! ',

    // add event
    'add.title' => 'Pridať stredoveko tématickú udalosť'  ,
    'add.desc' => 'Môžete pridať udalosť i keď ju neorganizujete. Môžete pridať bývalé udalosti..V prípade ak druh udalosti nie je v  možnostiach, prosím pridajte vlastný v popise v závere.',
    'add.contact.title' => 'Kontakt',
    'add.contact.name' => 'Vaše meno a priezvisko',
    'add.contact.email' => 'Váš-mail',
    'add.event.title' => 'Informácie o udalosti',
    'add.event.name' => 'Meno: Stredoveký festival v Paríži',
    'add.event.address' => 'Adresa udalosti : radnica, Kvetová ulica  3, atď',
    'add.event.city' => 'Mesto, kde sa koná udalosť',
    'add.event.country' => 'Štát',
    'add.event.start_date' => 'Začiatok',
    'add.event.start_end' => 'Koniec',
    'add.event.website' => 'Webová stránka',
    'add.event.type' => 'Druh udalosti',
    'add.event.cost' => 'Cena',
    'add.event.cost.label' => 'Zdarma, 2€, 3.5€ ak bez kostýmu...',
    'add.event.desc' => 'Úplný popis udalosti: účastníci, animácie, program, ukážka. Ak je to turnaj, prosím špecifikujte druh: súbojový...',

    // globals
    'cancel' => 'Zrušiť',
    'save' => 'Uložiť',

    'pun' => 'You don\'t speak borovička ? ',

];
