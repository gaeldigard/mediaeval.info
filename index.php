<?php

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  // Require composer autoloader
  require __DIR__ . '/app/vendor/Router.php';
  require __DIR__ . '/app/lib/controllers/Api.php';
  require __DIR__ . '/app/lib/controllers/Event.php';
  require __DIR__ . '/app/vendor/Translation.php';

  use Translation\Translation as T;

  $lang = 'en';
  if(isset($_GET['lang'])){
    T::forceLanguage($_GET['lang']);
    $lang = $_GET['lang'];
  }

  // Create Router instance
  $router = new \Bramus\Router\Router();

  //
  // Define routes
  //

  //home
  $router->get('/', function() use ($lang) {
    require __DIR__ . '/app/views/index.php';
  });

  //
  // add event
  //

  // show form
  $router->get('/ajouter', function() use ($lang){
    require __DIR__ . '/app/views/add.php';
  });

  // save form
  $router->post('/save', 'EventController@add');

  //
  // API
  //
  $router->get('/api/events/all', 'Api@getAllEvents');
  $router->get('/api/events/date/([0-9_-]+)', 'Api@getEventsBetweenDates');
  $router->get('/api/events/keyword/([^/]+)', 'Api@getEventsByKeyword');
  $router->get('/api/events/type/([0-9]+)', 'Api@getEventsByType');
  $router->get('/api/types/all', 'Api@getAllTypes');

  //
  // manage / admin
  //
  $router->get('/manage/validate/943724ZljnjkFZEFZNVJJHEjbkjhbmlnvA/([0-9]+)', 'EventController@validate');

  // about
  $router->get('/about', function() {
      require __DIR__ . '/app/views/about.php';
  });

  // debug
  $router->get('/debug/updateEventsPosition', 'EventController@updateEventsPosition');

  // Run it!
  $router->run();

?>
